using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using minicarsalesAngular.Models;


namespace minicarsalesAngular.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [Produces("application/json")]
    [Route("api/Cars")]
    public class CarsController : Controller
    {
        private readonly VehicleContext _context;

        public CarsController(VehicleContext context)
        {
            _context = context;

            if (_context.Cars.Count() == 0)
            {
                _context.Cars.Add(new Car { id = 1, Make = "Honda", Model = "Civic", CarType = typesOfCar.Hatchback, Doors = 5, Wheels = 4, PublishedDate = DateTime.Now });
                _context.Cars.Add(new Car { id = 2, Make = "Honda", Model = "Accord", CarType = typesOfCar.Sedan, Doors = 4, Wheels = 4, PublishedDate = DateTime.Now });
                _context.Cars.Add(new Car { id = 3, Make = "Honda", Model = "Legend", CarType = typesOfCar.Sedan, Doors = 2, Wheels = 4, PublishedDate = DateTime.Now });
                _context.SaveChanges();
            }
        }

        // GET: api/Cars
        [HttpGet]
        public IEnumerable<Car> GetCars()
        {
            return _context.Cars.ToList();
        }

        // GET: api/Cars/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var car = await _context.Cars.SingleOrDefaultAsync(m => m.id == id);

            if (car == null)
            {
                return NotFound();
            }

            return Ok(car);
        }

        // PUT: api/Cars/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCar([FromRoute] int id, [FromBody] Car car)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != car.id)
            {
                return BadRequest();
            }

            _context.Entry(car).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Cars
        [HttpPost]
        public async Task<IActionResult> PostCar([FromBody] Car car)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Cars.Add(car);
            //_context.Cars.Add(new Car { id = 3, Make = "Honda", Model = "Legend", CarType = typesOfCar.Sedan, Doors = 2, Wheels = 4, PublishedDate = DateTime.Now });
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCar", new { id = car.id }, car);
        }

        // DELETE: api/Cars/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var car = await _context.Cars.SingleOrDefaultAsync(m => m.id == id);
            if (car == null)
            {
                return NotFound();
            }

            _context.Cars.Remove(car);
            await _context.SaveChangesAsync();

            return Ok(car);
        }

        private bool CarExists(int id)
        {
            return _context.Cars.Any(e => e.id == id);
        }
    }
}