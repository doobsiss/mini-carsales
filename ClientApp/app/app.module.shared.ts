import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './components/app/app.component'
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { CarComponent } from './components/cars/car.component';
import { CarDetailComponent } from './components/cars/car-detail-component';

export const sharedConfig: NgModule = {
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        CarComponent,
        CarDetailComponent,
        HomeComponent
    ],
    imports: [
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'car-list', component: CarComponent },
            { path: 'car-edit', component: CarDetailComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
};
