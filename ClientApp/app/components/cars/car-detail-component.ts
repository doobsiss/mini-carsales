﻿import { Component, Inject, Input } from '@angular/core';
import { Car } from './car.component';

@Component({
    selector: 'car-detail',
    template: `
    <div *ngIf="car">
      <h2>{{car.make}} {{car.model}}</h2>
      <div>
        <label>Make: </label>
        <input [(ngModel)]="car.make" placeholder="make"/>
        <br/>
        <label>Model: </label>
        <input [(ngModel)]="car.model" placeholder="model"/>
        <br/>
        <label>Doors: </label>
        <input [(ngModel)]="car.doors" placeholder="doors"/>
        <br/>
        <label>Wheels: </label>
        <input [(ngModel)]="car.wheels" placeholder="wheels"/>
        <br/>
        <label>Type: </label>
        <input [(ngModel)]="car.type" placeholder="type"/>
        <br/>
      </div>
      <button>Save</button>
    </div>
    `
})

export class CarDetailComponent {
    @Input() car: Car;
}