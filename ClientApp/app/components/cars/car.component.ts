﻿import { Component, Inject, Input } from '@angular/core';
import { Http } from '@angular/http';
import { Modal } from 'bootstrap';

@Component({
    selector: 'cars',
    templateUrl: './car.component.html'
})
export class CarComponent {
    public cars: Car[];

    constructor(http: Http, @Inject('ORIGIN_URL') originUrl: string) {
        http.get(originUrl + '/api/cars').subscribe(result => {
            console.log("Cars",result);
            this.cars = result.json() as Car[];
        });
    }

    selectedCar: Car;

    onSelect(car: Car): void {
        this.selectedCar = car;
        console.log("Car is: ", car);
    }
}

export class Car {
    id: number;
    make: string;
    model: number;
    wheels: number;
    doors: string;
    type: string;
    publishedDate: string;
}
