﻿using System;
using Microsoft.EntityFrameworkCore;

namespace minicarsalesAngular.Models
{
    public class Vehicle
    {
        public int id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Wheels { get; set; }
        public DateTime PublishedDate { get; set; }
    }

    public class VehicleContext : DbContext
    {
        public VehicleContext(DbContextOptions<VehicleContext> options)
            : base(options)
        {
        }

        public DbSet<Car> Cars { get; set; }
        public DbSet<Bike> Bikes { get; set; }



        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=vehicles.db");
        }
        */
    }

    public enum typesOfCar
    {
        Hatchback,
        Sedan
    }

    public class Car : Vehicle
    {
        public int Doors { get; set; }

        private typesOfCar currentCarType;

        public typesOfCar CarType
        {
            get
            {
                return currentCarType;
            }
            set
            {
                currentCarType = value;
            }
        }
    }

    public class Bike : Vehicle
    {
        public enum typesOfMotorbike
        {
            Road,
            Offroad
        }

        private typesOfMotorbike currentBikeType;

        public typesOfMotorbike BikeCarType
        {
            get
            {
                return currentBikeType;
            }
            set
            {
                currentBikeType = value;
            }
        }
    }
}